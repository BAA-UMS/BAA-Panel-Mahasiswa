<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pengajuan extends CI_Model
{
    public function __construct()
    {
        parent::__construct();

        $this->mysql = $this->load->database('mysql', TRUE);
    }

    public function getPerubahanStatusKategori()
    {
        $sql = "SELECT * FROM ref_kategori";

        $result = $this->mysql->query($sql);

        return $result;
    }

    public function getDataGolongan()
    {
        $sql = "SELECT golongan_kode as ID, golongan_nama as KET FROM ref_golongan";

        $result = $this->mysql->query($sql);

        return $result;
    }

    public function getDataRiwayatPengajuanMhsAktif($nim)
    {
        $sql = "SELECT pmhsaktif_id as 'riwayat_id',
    				   nim,	
    				   pmhsaktif_keperluan AS 'keperluan',
    				   pmhsaktif_cetak AS 'proses_1',
    				   pmhsaktif_ttd AS 'proses_2',
    				   DATE_FORMAT(created_at, '%d/%m/%Y') AS 'tanggal'
    			FROM pengajuan_mhs_aktif
                WHERE nim = '$nim' ";

        $result = $this->mysql->query($sql);

        return $result;
    }

    public function getDataRiwayatPerubahanStatusMhs($nim)
    {
        $sql = "SELECT pmhsstatus_id as 'riwayat_id',
    				   nim,	
    				   pmhsstatus_keterangan AS 'keperluan',
    				   pmhsstatus_prodi as 'proses_1',
    				   pmhsstatus_perpus AS 'proses_2',
    				   pmhsstatus_bak AS 'proses_3',
    				   pmhsstatus_baa AS 'proses_4',
    				   pmhsstatus_cetak_surat AS 'proses_5',
    				   pmhsstatus_ttd AS 'proses_6',
    				   DATE_FORMAT(created_at, '%d/%m/%Y') AS 'tanggal'
    			FROM pengajuan_mhs_perubahanstatus
                WHERE nim = '$nim' ";

        $result = $this->mysql->query($sql);

        return $result;
    }

    public function addDataPerubahanStatusMhs($data, $ortu, $ods)
    {
        $massData['nim'] = $data['nim'];
        $massData['pmhsstatus_kategori_id'] = $data['pil_permohonan'];
        $massData['pmhsstatus_keterangan'] = $data['ket_perubahan'];
        $massData['pmhsstatus_scan_ortu'] = $ortu;
        if (!is_null($ods)) {
            $massData['pmhsstatus_scan_ods'] = $ods;
        }
        print_r($massData);die();
        $result = $this->mysql->insert('pengajuan_mhs_perubahanstatus', $massData);

        return $result;
    }

    public function addDataMahasiswaAktif($data)
    {
    	// print_r($data['pmhsaktif_golongan_id']);
    	// die();
    	$massData['nim'] = $data['nim'];
    	$massData['pmhsaktif_keperluan'] = $data['mhs_keperluan'];
    	$massData['pmhsaktif_nm_ortu'] = $data['mhs_ortu_nama'];
    	$massData['pmhsaktif_instansi'] = $data['mhs_ortu_instansi'];

		if (array_key_exists('mhs_ortu_pangkat', $data)) {
		    $a = $this->mysql->select('golongan_id')
                             ->from('ref_golongan')
		    				 ->where('golongan_kode', $data['mhs_ortu_pangkat'])
		    				 ->get();

		    $massData['pmhsaktif_golongan_id'] = $a->row()->golongan_id;
            $massData['pmhsaktif_nik'] = $data['mhs_ortu_no_id'];

		}else{
			$massData['pmhsaktif_pensiunan'] = $data['mhs_ortu_pensiunan'];
            $massData['pmhsaktif_norek']= $data['mhs_ortu_no_rek'];

		}
    	
    	$result = $this->mysql->insert('pengajuan_mhs_aktif', $massData);

        return $result;
    	
    }

    public function removeDataRiwayatMahasiswaAktif($data)
    {
        // $massData['nim'] = $data['nim'];
        $massData['pmhsaktif_id'] = $data['riwayatID'];

        $this->mysql->delete('pengajuan_mhs_aktif', $massData);
        return $result;
    }

    public function removeDataRiwayatPerubahanStatusMhs($data)
    {
        // $massData['nim'] = $data['nim'];
        $massData['pmhsstatus_id'] = $data['riwayatID'];

        $this->mysql->delete('pengajuan_mhs_perubahanstatus', $massData);
        // return $result;
    }
}