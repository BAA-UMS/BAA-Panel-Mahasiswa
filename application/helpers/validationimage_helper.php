<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


function validImage($image)
{
    return ((($image["type"] == "image/gif") || ($image["type"] == "image/jpeg") ||
            ($image["type"] == "image/jpg") || ($image["type"] == "image/png")) &&
        ($image["size"] <= 1500000));//1.5 MB
}


function randomString($length = 6)
{
    $str = "";
    $characters = array_merge(range('A', 'Z'), range('a', 'z'), range('0', '9'));
    $max = count($characters) - 1;
    for ($i = 0; $i < $length; $i++) {
        $rand = mt_rand(0, $max);
        $str .= $characters[$rand];
    }
    return $str;
}

function formatImageName($image, $prefix = '')
{
    $new_name = str_replace(" ", "-", $image['name']);
    $new_name = $prefix . '_' . randomString(5) . '_' . $new_name;
    return $new_name;
}