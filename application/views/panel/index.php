<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<!-- Android -->
    <!-- <meta name="theme-color" content="#4A2FC4"> -->
    <!-- Win Phone -->
    <!-- <meta name="msapplication-navbutton-color" content="#4A2FC4"> -->
    <!-- IOS -->
    <!-- <meta name="apple-mobile-web-app-status-bar-style" content="#4A2FC4"> -->
    <meta content='width=device-width, initial-scale=0.8, maximum-scale=1.0, user-scalable=0' name='viewport'/>
	<title>TITLE</title>

	<link rel="icon" type="image/png" href="/assets/images/system/ums-icon.png">
	<link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' rel="stylesheet">
</head>
<body>	
	<div id="app"></div>
	<script type="text/javascript" src="assets/js/adminpanel.js"></script>
	<link rel="stylesheet" href="assets/css/adminpanel.css">
</body>
</html>