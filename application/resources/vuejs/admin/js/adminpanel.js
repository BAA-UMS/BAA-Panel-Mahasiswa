/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap')

// css
import VueCSS from 'vuetify/dist/vuetify.css'

// Lib import
import Vue from 'vue'
import Vuetify from 'vuetify'
import Toasted from 'vue-toasted'
import VueRouter from 'vue-router'
// import VueLocalStorage from 'vue-ls'
import VueSweetAlert from 'vue-sweetalert'


// router and initial setup
import routes from './routes/routes'
import App from './src/Main.vue'

// Options VueLocalStorage
// let options = {
//     namespace: 'vuejs__'
// };


// use library
Vue.use(Vuetify)
Vue.use(Toasted)
Vue.use(VueRouter)
Vue.use(VueSweetAlert)
// Vue.use(VueLocalStorage, options);

Vue.component('vue-loader', require('vue-spinner/src/ClipLoader.vue'))

// configure router
const router = new VueRouter({
    routes,
})

router.beforeEach((to, from, next) => {
    document.title = to.meta.title
    next()
})

new Vue({
    el: '#app',
    router,
    render: h => h(App),
})
