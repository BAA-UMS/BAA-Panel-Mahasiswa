// 404 view
import NotFound from '../src/notFound.vue'
// Dashboard
import Dashboard from '../src/dashboard/dashboard.vue'
// Pengajuan
import MainPengajuan from '../src/pengajuan/mainPengajuan.vue'
import PengajuanMhsAktif from '../src/pengajuan/pengajuanMahasiswaAktif.vue'
import PerubahanStatusMhs from '../src/pengajuan/pengajuanPerubahanStatusMhs.vue'
// Riwayat
import MainRiwayat from '../src/riwayat/mainRiwayat.vue'
import RiwayatMhsAktif from '../src/riwayat/riwayatPengajuanMhsAktif.vue'
import RiwayatPerubahanStatusMhs from '../src/riwayat/riwayatPerubahanStatusMhs.vue'

const routes = [
	{
		path: '/', 
		redirect: '/dashboard' 
	},
	{
		path: '/dashboard',
		name: 'dashboard',
		component: Dashboard,
		meta: {title: 'Home | BAA UMS'}
	},
	
	{
		path: '/pengajuan',
		component: MainPengajuan,
		redirect: '/pengajuan/mahasiswa-aktif',
		children: [
			{
				path: 'mahasiswa-aktif',
				name: 'pengajuan_mahasiswa_aktif',
				component: PengajuanMhsAktif,
				meta: {title: 'Pengajuan Mahasiswa Aktif | UMS - Universitas Muhammadiyah Surakarta'}
			},
			{
				path: 'perubahan-status-mahasiswa',
				name: 'pengajuan_perubahan_status_mhs',
				component: PerubahanStatusMhs,
				meta: {title: 'Perubahan Status Mahasiswa | UMS - Universitas Muhammadiyah Surakarta'}

			}
		]
	},
	{
		path: '/riwayat',
		component: MainPengajuan,
		redirect: '/riwayat/pengajuan-mhs-aktif',
		children: [
			{
				path: 'pengajuan-mhs-aktif',
				name: 'riwayat_mhs_aktif',
				component: RiwayatMhsAktif,
				meta: {title: 'Riwayat Pengajuan - Mahasiswa Aktif | UMS - Universitas Muhammadiyah Surakarta'}
			},
			{
				path: 'pengajuan-perubahan-status-mhs',
				name: 'riwayat_perubahan_status_mhs',
				component: RiwayatPerubahanStatusMhs,
				meta: {title: 'Riwayat Pengajuan - Perubahan Status Mahasiswa | UMS - Universitas Muhammadiyah Surakarta'}
			},
			
		]
	},
	{
		path: '/other',
		name: 'other',
		component: Dashboard,
		meta: {title: 'Home | BAA UMS'}
	},
	{ path: '*', component: NotFound }
]

export default routes
