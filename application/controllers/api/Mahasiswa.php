<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Pengajuan');
        $this->load->helper('validationimage_helper');
        // BASIC AUTH
        // $login => 'BAAUMS';
        // $pass => 'DEVBAA2017';

        // if(($_SERVER['PHP_AUTH_PW']!= $pass || $_SERVER['PHP_AUTH_USER'] != $login)|| !$_SERVER['PHP_AUTH_USER'])
        // {
        //     header('WWW-Authenticate: Basic realm="Login Relms"');
        //     header('HTTP/1.0 401 Unauthorized');
        //     echo 'Auth failed';
        //     exit;
        // }else{
        // 	// HARUS ADA CHECK SESSION JIKA BERHASIL
        // }
    }

    public function getInitialData()
    {
        $result = [
            'success' => true,
            'result'  => [
                'nim'     => 'L200140132',
                'picture' => 'assets/images/profile/1.jpg'
            ]
        ];

        $this->output
            ->set_content_type('application/json')
            ->set_header(201)
            ->set_output(json_encode($result));
    }

    public function getDataMahasiswa()
    {
        $result = [
            'success' => true,
            'result' => [
                'nama'          => 'Aldino Kemal Adi Gumawang',
                'nim'           => 'L200140132',
                'tempat_lahir'  => 'Boyolali',
                'tanggal_lahir' => '21/04/1997',
                'prodi'         => 'Informatika',
                'fakultas'      => 'Fakultas Komunikasi dan Informatika',
                'semester'      => '20171',
                'thn_akademik'  => '2017',
                'alamat'        => 'BSP 2 blok i no 4 Singkil Karanggeneng Boyolali',
                'no_hp'         => '089685024091',
            ]
        ];
        $this->output
            ->set_content_type('application/json')
            ->set_header(201)
            ->set_output(json_encode($result));
    }

    public function getDataGolongan()
    {
        // GET FROM MODEL
        $golongan = $this->Pengajuan->getDataGolongan();
        $golongan_aray = [];
        
        // foreach ($golongan->result() as $data) {
        //     array_push($golongan_aray, $data->golongan_nama);
        // }

        $result = [
            'success' => true,
            'result' => [
                'data_golongan' => $golongan->result(),
            ]
        ];

        $this->output
            ->set_content_type('application/json')
            ->set_header(201)
            ->set_output(json_encode($result));

    }

    public function getDataRiwayatPengajuanMhsAktif()
    {
        $nim ='L200140132';
        $riwayat = $this->Pengajuan->getDataRiwayatPengajuanMhsAktif($nim);

        $result = [
            'success' => true,
            'result'  => $riwayat->result_array()
        ];

        $this->output
            ->set_content_type('application/json')
            ->set_header(201)
            ->set_output(json_encode($result));
    }

    public function getKategoriPerubahan()
    {
        $kategori = $this->Pengajuan->getPerubahanStatusKategori();

        $result = [
            'success' => true,
            'result'  => $kategori->result_array()
        ];

        $this->output
            ->set_content_type('application/json')
            ->set_header(201)
            ->set_output(json_encode($result));
        // die();

    }

    public function getDataRiwayatPerubahanStatusMhs()
    {
        $nim = "L200140132";
        $riwayat = $this->Pengajuan->getDataRiwayatPerubahanStatusMhs($nim);

        $result = [
            'success' => true,
            'result'  => $riwayat->result_array()
        ];

        $this->output
            ->set_content_type('application/json')
            ->set_header(201)
            ->set_output(json_encode($result));
    }

    public function addDataMahasiswaAktif()
    {
        $data = $_REQUEST;
        $data['nim'] = 'L200140132';        
        // save to database
        $this->Pengajuan->addDataMahasiswaAktif($data);

        // response to frontend
        $result = [
            'success' => true,
            'data' => $data
        ];
        $this->output
            ->set_content_type('application/json')
            ->set_header(201)
            ->set_output(json_encode($result));
    }

    public function addDataPerubahanStatusMhs()
    {
        $scan_ortu = $_FILES['scan_image_ortu'];
        // validImage = helper
        if (validImage($scan_ortu)) {
            $nim = 'L200140132';
            $folderLocation = 'assets/images/mahasiswa/' . $nim;
            if (!is_dir($folderLocation)) {
                mkdir($folderLocation, 0777, TRUE);
            }

            // get data input and declare variable
            $dataInput = $_REQUEST;
            $dataInput['nim']='L200140132';            
            $format_name_scan_ortu = null;
            $format_name_scan_ods = null;

            // pindah data ke assets path
            $format_name_scan_ortu = formatImageName($scan_ortu, 'ortu');
            move_uploaded_file($scan_ortu['tmp_name'], $folderLocation . '/' . $format_name_scan_ortu);

            // cek apakah ada scan ods (pilihan pindah program studi)
            if (!empty($_FILES['scan_image_ods'])) {
                $scan_ods = $_FILES['scan_image_ods'];
                if (!validImage($scan_ods)) {
                    $result = [
                        'success' => false,
                        'message' => 'Gambar terlalu besar / format tidak sesuai'
                    ];
                    $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode($result));
                }

                // pindah data ke assets path
                $format_name_scan_ods = formatImageName($scan_ods, 'ods');
                move_uploaded_file($scan_ods['tmp_name'], $folderLocation . '/' . $format_name_scan_ods);

            }

            // Save to datbase            
            $this->Pengajuan->addDataPerubahanStatusMhs($dataInput, $format_name_scan_ortu, $format_name_scan_ods);

            $result = [
                'success' => true,
                'message' => 'Pengajuan berhasil'
            ];
            $this->output
                ->set_content_type('application/json')
                ->set_header(201)
                ->set_output(json_encode($result));
        } else {
            $result = [
                'success' => false,
                'message' => 'Gambar terlalu besar / format tidak sesuai'
            ];
            $this->output
                ->set_content_type('application/json')
                ->set_header(201)
                ->set_output(json_encode($result));
        }

    }
    public function removeDataRiwayatPengajuanMhsAktif()
    {
        $data = $_REQUEST;
        // remove database
        $this->Pengajuan->removeDataRiwayatMahasiswaAktif($data);

        // response to frontend
        $riwayat = $this->Pengajuan->getDataRiwayatPengajuanMhsAktif($data['nim']);

        $result = [
            'success' => true,
            'result'  => $riwayat->result_array()
        ];
        
        $this->output
            ->set_content_type('application/json')
            ->set_header(201)
            ->set_output(json_encode($result));
    }

    public function removeDataRiwayatPerubahanStatusMhs()
    {
        $data = $_REQUEST;
        // remove database
        $this->Pengajuan->removeDataRiwayatPerubahanStatusMhs($data);

        // response to frontend
        $riwayat = $this->Pengajuan->getDataRiwayatPerubahanStatusMhs($data['nim']);

        $result = [
            'success' => true,
            'result'  => $riwayat->result_array()
        ];
        
        $this->output
            ->set_content_type('application/json')
            ->set_header(201)
            ->set_output(json_encode($result));
    }
}

