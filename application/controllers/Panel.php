<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Panel extends CI_Controller {

	public function __construct() {
        parent::__construct();
        // $this->load->library('parser');
    }

	public function index()
	{
		// $this->load->view('panel/index');
		$this->load->library('parser');
        //entries blog...
        $entries = array(
                    array('title' => 'Hallo...', 'body' => 'Namaku Ikhsan'),
                    array('title' => 'Met Pagi...!', 'body' => 'Bagaimana kabar Anda hari ini ?'),
                    array('title' => 'Hai semua :)!', 'body' => 'Apakah Anda sudah sarapan hari ini?'),
                    );          

        //sisipkan index lain ke dalam array, kita beri nama no, comment, numcomment misalnya
        // foreach ($entries as $key => $entry)
        // {
        //     $no = $key+1;
        //     $entries[$key]['no'] = $no;
        //     $entries[$key]['comment'] = array(
        //                     array('nama'=>'samson'.$no,'comment'=>'comment'.$no),
        //                     array('nama'=>'ryan'.$no,'comment'=>'komentar'.$no)
        //                     );
        //     foreach($entries[$key]['comment'] as $keys => $val){
        //                 $entries[$key]['comment'][$keys]['numcomment'] = $keys + 1;
        //     }
        // }                            

        $this->parser->parse('panel/index', array('blog_entries' => $entries));
	}
}
