# Pengembangan BAA Pengajuan Mahasiswa Aktif #

## Langakah Pemakaian

1. Clone project ini
2. Pastikan install [NodeJS dan NPM](https://nodejs.org/en/download/) lalu lakukan ```npm install``` (karena project dikembangkan dengan syntax ES6 vue JS)
3. Lakukan ```npm run watch-poll``` untuk mengembangkan script vue js yang berlokasi ```application/resources/vuejs/namaFile.vue```
4. Masuk pada directory folder via terminal / CMD (**shift + right click** pada folder, kemudian open with **command prompt**)
5. Jalankan ```php -S 127.0.0.1:8000``` (agar url lebih ringkas, tidak harus berada pada htdocs), jika error php not recognize buka pada [step4](https://www.sitepoint.com/how-to-install-php-on-windows/)
6. Buka *localhost:8000*

## Jika anda menggunakan htdocs
* tambahkan file .htaccess > isikan seperti [snippet ini](https://gitlab.com/BAA-UMS/BAA-Panel-Mahasiswa/snippets/1687080)


## Jika masi terjadi error
* `npm install vue` dan `npm install vue-template-compiler`
* kemudian jalankan `npm run watch-poll` atau `npm run production`


## Note
`npm run watch-poll` : untuk development <br />
`npm run production` : untuk produksi (minify + obfuscade)
___

## Tampilan Perubahan Status Mahasiswa
!["Perubahan Status Mahasiswa"](https://image.ibb.co/evJt06/Screenshot_from_2017_11_19_19_47_04.png "Perubahan Status Mahasiswa")

## Tampilan Riwayat Pengajuan
!["Riwayat Pengajuan Surat"](https://preview.ibb.co/iyWqcm/Screenshot_from_2017_11_15_22_24_07.png "Riwayat pengajuan surat")
