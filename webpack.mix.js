const mix = require('laravel-mix');

mix.js('application/resources/vuejs/admin/js/adminpanel.js', 'assets/js')
	.sass('application/resources/vuejs/admin/sass/adminpanel.scss', 'assets/css')
	.setPublicPath('./');
